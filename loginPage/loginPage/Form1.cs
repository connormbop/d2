﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace loginPage
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void clientBtn_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            bool loggedIn = false;
            string username = "", password = "", fname = "", sname = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(usernameBox.Text.Trim()) || "".Equals(passBox.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/
                username = usernameBox.Text.Trim();
                password = passBox.Text.Trim();
            }
            catch (Exception ex)
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM CLIENT");
            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        fname = SQL.read[3].ToString();
                        sname = SQL.read[2].ToString();
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("There are no accounts registed");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                usernameBox.Focus();
                return;
            }
        }

        private void instructBtn_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            bool loggedIn = false;
            string username = "", password = "", fname = "", sname = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(usernameBox.Text.Trim()) || "".Equals(passBox.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/
                username = usernameBox.Text.Trim();
                password = passBox.Text.Trim();
            }
            catch (Exception ex)
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM INSTRUCTOR");
            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        fname = SQL.read[3].ToString();
                        sname = SQL.read[2].ToString();
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("There are no accounts registed");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                usernameBox.Focus();
                return;
            }
        }

        private void adminBtn_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            bool loggedIn = false;
            string username = "", password = "", fname = "", sname = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(usernameBox.Text.Trim()) || "".Equals(passBox.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/
                username = usernameBox.Text.Trim();
                password = passBox.Text.Trim();
            }
            catch (Exception ex)
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM ADMINISTRATOR");
            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        fname = SQL.read[3].ToString();
                        sname = SQL.read[2].ToString();
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("There are no accounts registed");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                usernameBox.Focus();
                return;
            }
        }
    }
}
